# pep-example

Un esempio completo e funzioannte del PEP basato su nginx.

I file contenuti nella directory pepexample:

build-image.sh crea un'immagine docker (usando Dockerfile) che comprende un nginx configurato a PEP e un server web python che fa da mock per backend. Il tutto parte come da file docker-entrypoint.sh.

pep.js il codice del PEP in JS.
config.js la configurazione del gatekeeper che mappa gli endpoint su l modello autorizzativo per il client che sul KC di dev è identificato come peptest.

nginx.conf e default.conf le configurazioni da passare a nginx. Da notare che il vhost si chiama pepexample quindi per risolvere correttamente il vhost va aggiunto al proprio file hosts una entry:

127.0.0.1   pepexample

docker-compose.yaml fa partire un'istanza del PEP + servizio sulla porta 80 e monta tutti i file come volumi in modo da poter sperimentare modificandoli e ricaricando nginx a caldo senza dover rigenerare sempre l'immagine.

Prima di lanciare il compose va esportata una variabile d'ambiente:

export pep_credentials={{basicauthd_del_pep_client_di_dev}}

Per fare delle prove si possono usare le seguenti curl sia cn basic auth che con Bearer token:

curl -u uname:pass http://peptest/operator?a=1
curl -u uname:pass http://peptest/operator -X POST -d"Some data"
curl http://peptest/operator -H "Authorization: Bearer {{TOKEN}}"