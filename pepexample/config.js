export default { config };

var config = {
    "hosts": [
        {
            "host": ["peptest"],
            "audience": "peptest",
            "allow-basic-auth": true,
            "paths": [
                {
                    "name": "operator",
                    "path": "^/operator/?.*$",
                    "methods": [
                        {
                            "method": "GET"
                        },
                        {
                            "method": "POST"
                        }
                    ]
                },
                {
                    "name": "researcher",
                    "path": "^/researcher/?.*$",
                    "methods": [
                        {
                            "method": "GET"
                        },
                        {
                            "method": "POST"
                        }
                    ]
                }
            ]
        }
    ]
}